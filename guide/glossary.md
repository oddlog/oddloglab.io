---
next: false
---

# Glossary

* A *logger* is an instance that provides methods to emit logs.
* A log *message* is the primary string of a log. This is comparable with traditional logs.
* A logging *level* is the severity of a log; higher is more severe.
* A log *payload* is some object with additional information about the state from which the log got emitted.
* A log *record* is an entry that arrived at its destination (e.g. a file, a stream, ...).
* A *child* logger is a logger that shares some information with its parent logger (logger scope, name prefix, options).
* A *transport* is an instance of a class that defines the processing of logs (e.g. write to file, pass to nested transports, ...).
* A *transformation* is a function that prepares some payload attribute for log records (e.g. strip unnecessary information).
* A logger *scope* contains information that is shared between loggers and their children as long as they are supposed to be closed as a unit.
