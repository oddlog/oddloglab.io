# Transports

## Definition

As a method to deliver logs to their targets - e.g. the console, files, or nested transports -, oddlog allows you to
specify as many transport targets per logger as you desire.

The [`createLogger`](./README.md#createlogger)/[`defineLogger`](./README.md#definelogger) methods accept a
`{(?Object)[]} transports` attribute for the logger options. Each entry specifies some target for the logs. Depending on
the attributes, a transport is selected. In case the attributes are not specific enough (they usually are), you may
specify a `{String} type` to manually select a transport:

```text
StreamTransport   type: "stream"
FileTransport     type: "file"
TriggerTransport  type: "trigger"
MemoryTransport   type: "memory"
FileTransport     type: "split"
```

## Types

Listed here is a overview over the types and some of their options. For a full list of options, take a look at the
[documentation](../documentation/transports.md).

Each Type accepts the following option:

| Option | Type | Description |
| --- | --- | --- |
| `level` | `Number|String` | default: `oddlog.DEBUG` - The minimum level of logs to process. |

### StreamTransport

Writes the log records to a stream.

| Option | Type | Description |
| --- | --- | --- |
| `stream` | `stream.Writable` | default: `process.stdout` - The stream to write to. |
| `closeStream` | `Boolean` | default: `false` - Whether to close the stream when the logger scope gets closed. |

### ConsoleTransport

Uses `console.*` methods to write logs using a customized format. This transport is intended for use within frontend
only. By default (`shy=true`), only logs with level `oddlog.WARN` and above are written. Use
[environment variables](./environment-variables.md) for more detailed logs.

### FileTransport

Writes the log records to a file.

| Option | Type | Description |
| --- | --- | --- |
| `path` | `String` | The file path to write to. |

### TriggerTransport

Caches logs until a trigger level is matched or exceeded by an incoming log. Whenever this triggers, the cached logs are
written to nested transports.

This transport is useful to keep logs bundled and thus provides an easy way to debug - by checking the log stack
directly leading to the triggering log.

| Option | Type | Description |
| --- | --- | --- |
| `transports` | `(?Object)[]` | A list of nested transport definitions. |
| `trigger` | `Number|String` | default: `oddlog.ERROR` - The minimum level to trigger a cache flood to nested transports. |
| `limit` | `Number` | default: `100` - The cache size (in logs). |
| `replicate` | `Boolean|Number` | default: `false` - If `true`, create dedicated caches for children. If a number is provided, create dedicated caches for children up of that depth. |

### MemoryTransport

Appends the log instances to the provided store. Useful for testing.

| Option | Type | Description |
| --- | --- | --- |
| `store` | `{push:Function(Log)}` | default: `[]` - The store to push logs to. |

### SplitTransport

Decides to which nested transport to pass a log to, according to specified `split` levels.

| Option | Type | Description |
| --- | --- | --- |
| `transports` | `(?Object)[]` | A list of nested transport definitions. Every log is only passed to a single nested transport. |
| `split` | `(?Number|String)[]` | The list of split threshold levels to decide which transport to use. E.g. `level < split[0]` will use the first transport, `split[0] <= level < split[1]` the second, ... |
