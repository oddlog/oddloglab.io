---
prev: false
---

# Introduction

## What is oddlog?

Oddlog is a sophisticated and performance-optimized logging library for node.js. It is suited for libraries as well as
applications. The main concept is to not only log strings but to log whole objects in addition; this provides much more
information for debugging and monitoring purposes.

### Why to choose oddlog?

One pattern that on other javascript logger did before us is the in-process bundling of logs to a stack that is only
written when some log level threshold triggers. The [TriggerTransport](./transports.md#triggertransport) of oddlog does
exactly that. As long as no error (for example) occurs, nothing is logged and even intensive logging performs excellent.
When logs are not written but discarded (either no error occurs at all or cache limit is reached), their demanding
payload merge and stringify operations are never called. Most applications would do fine without intensive logging when
nobody would read the logs anyways. This bundling mechanism also enhances the debugging process due to logs being
structured like stack traces instead of a mixed bag.

::: tip
Speed up the debugging process and improve the performance of your application by simply using [TriggerTransports](./transports.md#triggertransport). You don't need all the logs.
:::

## When to use?

In many **applications** a simple string based logging approach does not provide enough information to debug any errors
post-mortem. For those applications you'll want to use payload-based logging instead. Oddlog makes this kind of logging
very simple and provides advanced mechanisms for debugging.

For **libraries** that are large enough for logging to make sense at all, oddlog is a good match as it provides a simple
flag (`shy`) to raise threshold levels to `WARN` and thus replace logging methods below with no-operations. Debugging
with the `shy`-flag enabled is as easy as setting up an [environment variable](./environment-variables.md).

## Alternatives

### Context

Since we are only aware of [bunyan](https://www.npmjs.com/package/bunyan) (version 1.x) and
[pino](https://www.npmjs.com/package/pino) (version 4.x) as comparable contestants to oddlog, we will take direct
comparisons and speak out recommendations based on those.

### Bunyan is superseded

Both oddlog and pino are meant to supersede bunyan of that they were inspired by. So despite being an innovative logger
for javascript, we consider bunyan obsolete as it does not provide any value over oddlog or pino (in the context of
node.js). The last reason to use bunyan in favor of the others might be available transport plugins. In this case,
please file an [issue](https://gitlab.com/oddlog/transports/issues) so the demand is noticed.

This logging library has been highly inspired by bunyan. A big **thank you** to its author!

### Pino vs oddlog

Our [benchmarks](./benchmarks.md) show that pino and oddlog perform comparable well; one better in some cases, the other
one in other cases. There is no clear winner here. However for intensive logging (in contrast to inheritance), pino
outperforms oddlog by design, as pino does not merge the payloads as objects but rather appends attributes with string
operations.
