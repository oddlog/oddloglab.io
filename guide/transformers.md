# Transformers

Transformers allow to replace the values of specified keys within payloads on-demand. Oddlog uses a few helpful
transformers by default: `fn`, `err`, `req`, `res` (see [documentation](../documentation/transformers.md)).

Transformers can be passed as option to
[`createLogger`](./README.md#createlogger)/[`defineLogger`](./README.md#definelogger). The standard transformers are
exported as `oddlog.stdTransformer`; this way one can use them in addition to custom ones:

```javascript
oddlog
  .createLogger("my-app", {
    transformer: oddlog.assign(
      { hex: (x) => x.toString(16).toUpperCase() },
      oddlog.stdTransformer
    )
  })
  .info({hex: 255}); // logs with payload `{"hex": "FF"}`
```
