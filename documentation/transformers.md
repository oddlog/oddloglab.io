---
prev: false
next: false
---

# Transformers

## Function

Replaces functions at the `fn` key of payloads with an object such as

```text
fn : {
  type: "function",
  name: value.name,
  length: value.length
}
```

## Error

Replaces errors at the `err` key of payloads with an object such as

```text
err : {
  message: err.message,
  name: err.name,
  code: err.code,
  stack: err.stack
}
```

## Request (IncomingMessage)

Replaces requests at the `req` key of payloads with an object such as

```text
req : {
  method: value.method,
  url: value.url,
  headers: value.headers,
  remoteAddress: value.connection.remoteAddress,
  remotePort: value.connection.remotePort
}
```

## Response (ServerResponse)

Replaces responses at the `res` key of payloads with an object such as

```text
res : {
  statusCode: value.statusCode,
  headers: value._header
}
```
