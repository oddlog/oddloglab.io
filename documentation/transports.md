---
prev: false
next: false
---

# Transports

## Types

### StreamTransport

*Inherits:* [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `stream` | `stream.Writable` | default: `process.stdout` - The stream to write to. |
| `closeStream` | `Boolean` | default: `false` - Whether to close the stream when the logger scope gets closed. |
| `format` | `String|{String}Function<log>` | default: `"raw"` - The record format to use or a formatting function. Format options: `"raw"`, `"simple"`. |
| `separator` | `String` | default: `os.EOL` - String to append to every log record. |
| `encoding` | `?String` | If set, `stream.setDefaultEncoding(encoding)` is called. |

With `shy` set the `format` defaults to `"simple"` (does not apply to [FileTransport](#filetransport)).

### ConsoleTransport

*Inherits:* [BasicTransport](#basictransport)

The `shy` option defaults to `true`.

### FileTransport

*Inherits:* [StreamTransport](#streamtransport), [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `path` | `String` | The file path to write to. |

Does not accept inherited options: `stream` (created by `path`) and `closeStream` (always `true`).

### SplitTransport

*Inherits:* [NestedTransport](#nestedtransport), [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `split` | `(?Number|String)[]` | The split level thresholds. Each log is only passed to a single nested `transports` according to the split-points. Transport index `i` is used for `split[i-1] <= level < split[i]` with `split[-1] := 0`. |

### TriggerTransport

*Inherits:* [NestedTransport](#nestedtransport), [DeferredTransport](#deferredtransport), [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `trigger` | `Number|String` | default: `oddlog.ERROR` - The minimum level to trigger cache flooding. |
| `guard` | `Boolean` | default: `true` - Whether to wrap the cached logs with guard logs (`--- start/end cache ---`) on flooding. |
| `replicate` | `Boolean|Number` | default: `false` - If `true`, create dedicated caches for children. If a number is provided, create dedicated caches for children up of that depth. |
| `window` | `Boolean|Number` | default: `limit >= 4096` - If `false`, use a [RingCache](./cache.md#ringcache). Otherwise use a [RayCache](./cache.md#raycache). |

The TriggerTransport uses a [cache](./cache.md) with its type depending on the `window` option. Additional cache options
(depending on the type) can be set.

**Events** are emitted on the logger with `transport:trigger:` prefix as following:

| Name | Arguments | Trigger |
| --- | --- | --- |
| `flooding:start` | `{Number} id`, `{Object} payload` | Whenever a flooding gets started. |
| `flooding:end` | `{Number} id`, `{Object} payload` | Whenever a flooding got completed. |

### MemoryTransport

*Inherits:* [DeferredTransport](#deferredtransport), [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `store` | `{push:Function(Message)}` | default: `new cache` - The store to push logs to. |
| `limit` | `Number` | default: `Infinity` - The `limit` cache option (default is altered). |
| `window` | `Boolean|Number` | default: `limit >= 4096` - If `false`, use a [RingCache](./cache.md#ringcache). Otherwise use a [RayCache](./cache.md#raycache). |

The MemoryTransport uses a [cache](./cache.md) with its type depending on the `window` option. Additional cache options
(depending on the type) can be set.

### VoidTransport

Ignores all options. When used, it is a singleton instance. Does ignore everything.

## Base classes

### BasicTransport

*Inherited by:* All built-in transports (except for [VoidTransport](#voidtransport)).

A class to provide most basic transport options.

| Option | Type | Description |
| --- | --- | --- |
| `shy` | `Boolean` | default: `logger.shy` - Whether to use sane defaults for libraries instead of applications. |
| `level` | `Number|String` | default: `shy ? oddlog.WARN : oddlog.DEBUG` - The minimum level of logs to process. |

*Note:* The `level` option is ignored if a matching [environment variable](../guide/environment-variables.md) is available.

### DeferredTransport

*Inherited by:* [TriggerTransport](#triggertransport)

A class to be inherited by transports that may not deliver their logs immediately (e.g. internal cache). It provides
options to tweak the laziness in such cases.

| Option | Type | Description |
| --- | --- | --- |
| `immediateFormatting` | `Boolean` | default: `false` - Whether to immediately apply log message formatting. |
| `immediateOwnership` | `Boolean` | default: `true` - Whether to immediately shallow copy payloads that oddlog may not modify (see [performance considerations](../guide/performance-considerations.md#specify-permissions-for-oddlog). |
| `immediateTransform` | `Boolean` | default: `false` - Whether to immediately merge payloads (implies `immediateOwnership`). |
| `immediateStringify` | `Boolean` | default: `false` - Whether to immediately create the raw message string (implies `immediateTransform`). |
| `immediateFreeze` | `Boolean` | default: `false` - Whether to immediately deep clone the payload; do not use unless you know what you are doing (significant performance impacts). |

### NestedTransport

*Inherited by:* [SplitTransport](#splittransport), [TriggerTransport](#triggertransport)

A class to be inherited by transports that have nested transports to forward messages to.

| Option | Type | Description |
| --- | --- | --- |
| `transports` | `(?Object)[]` | A list of nested transport definitions. |

## Custom Transports

Custom transports must implement the following methods:

* `write({Message} log)` - Called on each log action.
* `{Number} minLevel()` - Should return the minimum log level that is processed.
* `{Transport} child({boolean} hasDedicatedScope)` - Called when a child logger is created. `hasDedicatedScope`
   specifies whether the child logger has a dedicated logger scope. This method must return a Transport (may be the
   identity).

In addition the following optional method can be implemented:

* `attach({Logger} logger)` - Called when the transport is being attached to a logger.

::: tip
When `Transport#attach()` is implemented, it probably makes no sense to return the identity within `Transport#child()`.
:::

Also keep in mind that you may want to inherit from any of the built-in [base classes](#base-classes) above.

You may use a custom transport instance via `{instance: myTransport}` in place of any transport definition. However it
is preferred to publish those as [plugins](../guide/plugins.md) instead if they are re-usable by others.
