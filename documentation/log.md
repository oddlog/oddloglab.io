---
prev: false
next: false
---

# Class: Log

The class for log entries. Parses arguments passed to [logging methods](./logger.md#logging-methods) and creates the
record strings.

## Attributes

### logger

The logger that created this log.

**Type:** [`Logger`](./logger.md)

### level

The severity level. See [logging levels](./under-the-hood.md#logging-levels).

**Type:** `Number`

### date

The creation date (timestamp).

**Type:** `Number`

## Methods

### gainPayloadOwnership

Ensures that the assigned payload is owned by oddlog.

**Returns:** `undefined`

### getFormattedMessage

Applies formatting to the primary message and caches it.

**Returns:** `String|undefined`

### getPayload

Merges all inherited payloads with the log-specific payload and caches it.

**Returns:** `?Object|undefined`

### getRawRecordString

Creates and caches the [raw record string](./under-the-hood.md#raw-log-record).

**Returns:** `String`

### getSimpleRecordString

Creates and caches a simple human readable record string.

**Returns:** `String`

### getRecordString

Calls any formatting method according to the passed `format`.

| Parameter | Type | Description |
| --- | --- | --- |
| `format` | `String|{String}Function<log>` | The record format to use or a formatting function. Format options: `"raw"`, `"simple"`. |

**Returns:** `String`
