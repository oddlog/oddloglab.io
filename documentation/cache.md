---
prev: false
next: false
---

# Cache

## RayCache

This cache type has a low memory footprint but bad performance.

The ray cache structure appends incoming logs to an array. The array size grows as more logs are appended. When
`limit + window` size is reached, the first `window` logs are spliced from the array.

| Option | Type | Description |
| --- | --- | --- |
| `limit` | `Number` | default: `100` - The amount of logs to keep (`window` additional logs can be present within cache; the discard-window). |
| `window` | `Number` | default: `max(5, limit/20)` - The amount of additional cache size in order to discarding multiple logs at once (for performance improvements). |
| `cleanUp` | `Boolean` | default: `true` - Whether to clear the cache after each flood. |
| `silentWindow` | `Boolean` | default: `false` - Whether to skip the logs within discard-window from being flooded. This ensures that at most `limit + 1` logs are flooded at once. |
| `malloc` | `Number` | optional. If set, specifies the size of the initial cache array. |

## RingCache

This cache type has great performance but a high (pre-allocated array of `limit` size) memory footprint.

The ring cache structure pre-allocates an array only once for the maximum needed size. It saves two indices, one for
start and one for end position. On incoming logs the log is put at the end position and it is increased. The array is
used as a ring and on trigger all logs between start (inclusive) and end (exclusive) are flooded.

| Option | Type | Description |
| --- | --- | --- |
| `limit` | `Number` | default: `100` - The amount of logs to keep. |
| `cleanUp` | `Boolean` | default: `true` - Whether to clear the cache after each flood. |
| `freeUp` | `Boolean` | default: `cleanUp` - Whether to free up log references for garbage collection on cache clear after flooding. Disabling gives a (small) performance boost. |
