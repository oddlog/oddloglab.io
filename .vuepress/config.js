const pkg = require("../package.json");

module.exports = {
  base: "/",
  title: "oddlog",
  description: pkg.description,
  dest: "public",
  head: [
    ["link", {rel: "icon", href: "/favicon.ico"}]
  ],
  themeConfig: {
    repo: "https://gitlab.com/oddlog",
    docsRepo: "https://gitlab.com/oddlog/oddlog.gitlab.io",
    algolia: process.env.ALGOLIA_API_KEY ? {
      apiKey: process.env.ALGOLIA_API_KEY,
      indexName: "oddlog",
    } : null,
    editLinks: true,
    lastUpdated: true,
    nav: [
      {text: "Guide", link: "/guide/"},
      // {text: "Quick Start", link: "/templates/"},
      {text: "Documentation", link: "/documentation/"},
    ],
    sidebar: {
      "/guide/": [
        "glossary",
        "",
        "getting-started",
        "logging-methods",
        "inheritance",
        "environment-variables",
        "transports",
        "transformers",
        "design-decisions",
        "performance-considerations",
        "plugins",
        "benchmarks",
      ],
      "/documentation/": [
        "",
        "logger",
        "log",
        "logger-scope",
        "transports",
        "cache",
        "transformers",
        "under-the-hood",
      ],
      // "/templates/": [
      //   "",
      //   "application",
      //   "library",
      // ]
    },
    sidebarDepth: 3,
  },
};
